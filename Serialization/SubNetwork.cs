﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    [Serializable]
    public class SubNetwork
    {
        [XmlAttribute("name")] public String Name { get; set; }

        [XmlAttribute("type")] public String Type { get; set; }

        [XmlElement] public GenericParameter BitRate { get; set; }

        [XmlElementAttribute] public List<ConnectedAP> ConnectedAP { get; set; }
    }
}
