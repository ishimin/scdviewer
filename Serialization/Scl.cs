﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup.Localizer;
using System.Xml.Serialization;
using DTT = SCDviewer.Serialization.DataTypeTemplates;

namespace SCDviewer.Serialization
{
    [XmlRoot("SCL")]
    public class Scl : NodeBase
    {
        [XmlElement] public Header Header { get; set; }
        [XmlElement] public Communication Communication { get; set; }
        [XmlElementAttribute("IED")] public ObservableCollection<IED> IEDs { get; set; }
        [XmlElement("DataTypeTemplates")] public DTT.DataTypeTemplates DataTypeTemplates { get; set; }
        [XmlAttribute("version")] public String Version { get; set; }
        [XmlAttribute("revision")] public String Revision { get; set; }
        
        #region NodeBase
        public override string NodeText => this.Header.ID;
        public override IEnumerable<NodeBase> NodeChildren
        {
            get
            {
                return IEDs;
            }
        }
        #endregion

        public static T FindFirst<T>(object root, Predicate<T> criteria)
        {
            T result = default;

            var children = Scl.GetCollections<T>(root);
            if (root != null)
            foreach (var collection in children) {
                foreach(var child in collection)
                {
                    if (criteria(child)) return child;
                }
            }

            return result;
        }
        public static List<T> FindAll<T>(object root, Predicate<T> criteria)
        {
            List<T> result = default;

            var children = Scl.GetCollections<T>(root);
            if (root != null)
            foreach (var collection in children)
            {
                foreach (var child in collection)
                {
                        if (criteria(child))
                            if (result == null) result = new List<T>() { child };
                        else result.Add(child);
                    }
            }

            return result;
        }
        public static IEnumerable<IEnumerable<T>> GetCollections<T>(object obj)
        {
            var res = new List<IEnumerable<T>>();
            var children = new List<IEnumerable<object>>();
            if (obj != null)
            {
                var type = obj.GetType();
                foreach (var prop in type.GetProperties())
                {
                    if (typeof(IEnumerable<T>).IsAssignableFrom(prop.PropertyType))
                    {
                        var get = prop.GetGetMethod();
                        if (!get.IsStatic && get.GetParameters().Length == 0) // skip indexed & static
                        {
                            var collection = (IEnumerable<T>)get.Invoke(obj, null);
                            if (collection != null && collection.Count() > 0) res.Add(collection);
                        }
                    }
                    else if (typeof(IEnumerable<object>).IsAssignableFrom(prop.PropertyType))
                    {
                        var get = prop.GetGetMethod();
                        if (!get.IsStatic && get.GetParameters().Length == 0) // skip indexed & static
                        {
                            var collection = (IEnumerable<object>)get.Invoke(obj, null);
                            if (collection != null) children.Add(collection);
                        }
                    }
                }
                foreach (var child in children)
                {
                    foreach (var collection in child)
                    {
                        var collesctions = Scl.GetCollections<T>(collection);
                        res.AddRange(collesctions);
                    }
                }
            }
            return res;
        }
    }
}
