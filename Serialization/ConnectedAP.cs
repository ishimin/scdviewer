﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    public class ConnectedAP
    {
        [XmlAttribute("iedName")] public String IEDName { get; set; }

        [XmlAttribute("apName")] public String APName { get; set; }

        [XmlElement("Address")] public Address Address { get; set; }

        [XmlElementAttribute("GSE")] public List<AddressGSE> GSE { get; set; }

        [XmlElement("PhysConn")] public PhysicalConnection PhysConn { get; set; }

        public override string ToString()
        {
            return String.Format("IEDName: \"{0}\", APName: \"{1}\"", IEDName, APName);
        }
    }
}
