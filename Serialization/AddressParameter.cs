﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    [Serializable]
    public class AddressParameter
    {
        [XmlAttribute("type")] public string Type { get; set; }
        [XmlText] public string Value { get; set; }

        public override string ToString()
        {
            return String.Format("{0}: \"{1}\"", Type, Value);
        }
    }
}
