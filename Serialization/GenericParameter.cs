﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    [Serializable]
    public class GenericParameter
    {
        [XmlAttribute("unit")] public String Unit { get; set; }

        [XmlAttribute("multiplier")] public String Mult { get; set; }
        
        [XmlText] public int Value { get; set; }
        public override string ToString()
        {
            return String.Format("Unit: \"{0}\" Mult: \"{1}\" Value:\"{2}\"", Unit, Mult, Value);
        }
    }
}
