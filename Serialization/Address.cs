﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    [Serializable]
    public class Address
    {
        [XmlElementAttribute("P")] public List<AddressParameter> Parameters { get; set; }

        public AddressParameter TryGetAddress(string key)
        {
            AddressParameter result = null;

            foreach (var p in Parameters)
                if (p.Type == key)
                {
                    result = p;
                    break;
                }

            return result;
        }
    }
}
