﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    public class AddressGSE
    {
        [XmlAttribute("ldInst")] public String IDInst { get; set; }

        [XmlAttribute("cbName")] public String CBName { get; set; }

        [XmlElement] public Address Address { get; set; }

        [XmlElement("MinTime")] public GenericParameter MinTime {get; set;}

        [XmlElement("MaxTime")] public GenericParameter MaxTime { get; set; }
    }
}
