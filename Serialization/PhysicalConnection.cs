﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    public class PhysicalConnection
    {
        [XmlAttribute("type")] public string Type { get; set; }
        [XmlElementAttribute("P")] public List<AddressParameter> Parameters { get; set; }

    }
}
