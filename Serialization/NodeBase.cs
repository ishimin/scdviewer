﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    public abstract class NodeBase
    {
        [XmlIgnore]
        public abstract string NodeText { get; }
        [XmlIgnore]
        public abstract IEnumerable<NodeBase> NodeChildren { get; }
        public virtual void GrabInfo(Scl root) { }
        public virtual string Tag { get; }
    }
}
