﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    [Serializable]
    public class LNodeType
    {
        [XmlAttribute("id")] public String ID { get; set; }
        [XmlAttribute("desc")] public String Desc { get; set; }
        [XmlAttribute("cdc")] public String CDC { get; set; }
        [XmlAttribute("lnClass")] public String InClass { get; set; }
        [XmlAttribute("CodeClass")] public String CodeClass { get; set; }
        [XmlAttribute("DataCode")] public String DataCode { get; set; }
        [XmlElementAttribute("DO")] public List<DO> DOs { get; set; }
        [XmlElementAttribute("DA")] public List<DA> DAs { get; set; }
        [XmlElementAttribute("SDO")] public List<SDO> SDOs { get; set; }
        [XmlElementAttribute("BDA")] public List<BDA> BDAs { get; set; }
    }
}
