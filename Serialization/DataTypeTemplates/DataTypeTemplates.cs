﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    [Serializable()]
    public class DataTypeTemplates
    {
        [XmlElementAttribute("LNodeType")] public List<LNodeType> LNodeTypes { get; set; }
        [XmlElementAttribute("DOType")] public List<DOType> DOTypes { get; set; }
        [XmlElementAttribute("DAType")] public List<DAType> DATypes { get; set; }
        [XmlElementAttribute("EnumType")] public List<EnumType> EnumTypes { get; set; }

    }
}
