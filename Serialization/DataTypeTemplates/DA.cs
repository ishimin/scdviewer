﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    public class DA : NodeBase
    {
        [XmlAttribute("desc")] public String Desc { get; set; }
        [XmlAttribute("name")] public String Name { get; set; }
        [XmlAttribute("bType")] public String BType { get; set; }
        [XmlAttribute("type")] public String Type { get; set; }
        [XmlAttribute("valKind")] public String ValKind { get; set; }
        [XmlAttribute("dchg")] public bool DCHG { get; set; }
        [XmlAttribute("qchg")] public bool QCHG { get; set; }
        [XmlAttribute("dupd")] public bool DUPD { get; set; }
        [XmlAttribute("count")] public int Count { get; set; }
        [XmlAttribute("fc")] public String FC { get; set; }
        [XmlIgnore] public string TypeText => String.Format("type: {0}", BType);
        [XmlIgnore] public string FCText => String.Format("[{0}]", FC);
        [XmlElement("Val")] public string Value { get; set; }

        [XmlIgnore] public List<DAValue> ValueNodes = new List<DAValue>();
        public override string NodeText => Name;
        public override IEnumerable<NodeBase> NodeChildren => ValueNodes;
        public override string Tag => Desc;

        public class DAValue : NodeBase
        {
            public override string NodeText { get; }

            public override IEnumerable<NodeBase> NodeChildren => null;

            public DAValue(string text)
            {
                NodeText = text;
            }
        }
    }
}
