﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    public class Private
    {
        [XmlAttribute("type")] public string Type { get; set; }
        [XmlText] public string Data { get; set; }
    }
}
