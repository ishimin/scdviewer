﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    public class EnumType
    {
        [XmlAttribute("id")] public String ID { get; set; }

        [XmlElementAttribute("EnumVal")] public List<EnumVal> EnumVals { get; set; }
    }
}
