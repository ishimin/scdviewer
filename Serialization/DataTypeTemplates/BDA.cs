﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    public class BDA
    {
        [XmlAttribute("name")] public String Name { get; set; }
        [XmlAttribute("bType")] public String BType { get; set; }
        [XmlAttribute("type")] public String Type { get; set; }
        [XmlAttribute("valKind")] public String ValKind { get; set; }
        [XmlElement("Val")] public string Value { get; set; }
    }
}
