﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    public class DO
    {
        [XmlAttribute("type")] public string Type { get; set; }
        [XmlAttribute("name")] public string Name { get; set; }
        [XmlAttribute("desc")] public string Desc { get; set; }
        //[XmlAttribute("sr:refRequired", Prefix = "sr")] public bool RefRequired { get; set; }
        [XmlAttribute("refRequired")] public bool RefRequired { get; set; }
        [XmlElement("Private")] public Private Private { get; set; }
        [XmlElement("Val")] public string Value { get; set; }
    }
}
