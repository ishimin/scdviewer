﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    public class EnumVal
    {
        [XmlText] public String Value { get; set; }
        [XmlAttribute("ord")] public String Ord { get; set; }
        [XmlAttribute("desc")] public String Desc { get; set; }
    }
}
