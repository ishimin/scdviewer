﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    public class DAType
    {
        [XmlAttribute("id")] public String ID { get; set; }
        [XmlAttribute("DataCode")] public String DataCode { get; set; }
        [XmlElementAttribute("BDA")] public List<DA> BDAs { get; set; }
    }
}
