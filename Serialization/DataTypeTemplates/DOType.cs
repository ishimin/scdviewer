﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.DataTypeTemplates
{
    public class DOType
    {
        [XmlAttribute("id")] public String ID { get; set; }
        [XmlAttribute("desc")] public String Desc { get; set; }
        [XmlElementAttribute("SDO")] public List<SDO> SDOs{ get; set; }
        [XmlAttribute("cdc")] public String CDC { get; set; }
        [XmlAttribute("DataCode")] public String DataCode { get; set; }
        [XmlElementAttribute("DA")] public List<DA> DAs { get; set; }
    }
}
