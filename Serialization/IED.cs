﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using SCDviewer.Serialization.IEDMeta;
using SCDviewer.Serialization.IEDMeta.Services;

namespace SCDviewer.Serialization
{
    [Serializable()]
    public class IED : NodeBase
    {
        [XmlAttribute("name")] public string Name { get; set; }
        [XmlAttribute("desc")] public string Desc { get; set; }

        [XmlArray("Services")]
        [XmlArrayItem("DynAssociation", typeof(DynAssociation))]
        [XmlArrayItem("GetDirectory", typeof(GetDirectory))]
        [XmlArrayItem("GetDataObjectDefinition", typeof(GetDataObjectDefinition))]
        [XmlArrayItem("GetDataSetValue", typeof(GetDataSetValue))]
        [XmlArrayItem("DataSetDirectory", typeof(DataSetDirectory))]
        [XmlArrayItem("ConfDataSet", typeof(ConfDataSet))]
        [XmlArrayItem("ReadWrite", typeof(ReadWrite))]
        [XmlArrayItem("ConfReportControl", typeof(ConfReportControl))]
        [XmlArrayItem("GetCBValues", typeof(GetCBValues))]
        [XmlArrayItem("ConfLogControl", typeof(ConfLogControl))]
        [XmlArrayItem("ReportSettings", typeof(ReportSettings))]
        [XmlArrayItem("GSESettings", typeof(GSESetting))]
        [XmlArrayItem("GOOSE", typeof(Goose))]
        [XmlArrayItem("FileHandling", typeof(FileHandling))]
        [XmlArrayItem("ConfLNs", typeof(ConfLNs))]
        public List<ServiceBase> Services { get; set; }

        [XmlElementAttribute("AccessPoint")] public List<AccessPoint> AccessPoints { get; set; }

        #region NodeBase
        public override string NodeText => String.Format("{0} {1}", Desc, Name);

        public override IEnumerable<NodeBase> NodeChildren
        {
            get
            {
                return AccessPoints;
            }
        }
        #endregion
    }
}
