﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    [Serializable()]
    public class Header
    {
        [XmlAttribute("id")] public String ID { get; set; }
        [XmlAttribute("version")] public String version { get; set; }
        [XmlAttribute("revision")] public String revision { get; set; }
        [XmlAttribute("toolID")] public String toolID { get; set; }
        [XmlAttribute("nameStructure")] public String nameStructure { get; set; }
    }
}
