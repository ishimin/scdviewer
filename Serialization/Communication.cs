﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    [Serializable()]
    public class Communication
    {
        [XmlElementAttribute] public List<SubNetwork> SubNetwork { get; set; }
    }
}
