﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta
{
    public class IEDLink
    {
        [XmlAttribute("ldInst")] public string IdInst { get; set; }

        [XmlText] public string Link { get; set; }
    }
}
