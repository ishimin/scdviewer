﻿using SCDviewer.Serialization.IEDMeta.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta
{
    public class AccessPoint : NodeBase
    {
        [XmlAttribute("name")] public String Name { get; set; }
        [XmlAttribute("router")] public bool Router { get; set; }
        [XmlAttribute("clock")] public bool Clock { get; set; }
        [XmlAttribute("desc")] public string Desc { get; set; }

        [XmlElement] public Server Server { get; set; }

        [XmlElement] public ServerLink ServerAt { get; set; }

        [XmlArray("Services")]
        [XmlArrayItem("DynAssociation", typeof(DynAssociation))]
        [XmlArrayItem("GetDirectory", typeof(GetDirectory))]
        [XmlArrayItem("GetDataObjectDefinition", typeof(GetDataObjectDefinition))]
        [XmlArrayItem("GetDataSetValue", typeof(GetDataSetValue))]
        [XmlArrayItem("DataSetDirectory", typeof(DataSetDirectory))]
        [XmlArrayItem("ConfDataSet", typeof(ConfDataSet))]
        [XmlArrayItem("ReadWrite", typeof(ReadWrite))]
        [XmlArrayItem("ConfReportControl", typeof(ConfReportControl))]
        [XmlArrayItem("GetCBValues", typeof(GetCBValues))]
        [XmlArrayItem("ConfLogControl", typeof(ConfLogControl))]
        [XmlArrayItem("ReportSettings", typeof(ReportSettings))]
        [XmlArrayItem("GSESettings", typeof(GSESetting))]
        [XmlArrayItem("GOOSE", typeof(Goose))]
        [XmlArrayItem("FileHandling", typeof(FileHandling))]
        [XmlArrayItem("ConfLNs", typeof(ConfLNs))]
        public List<ServiceBase> Services { get; set; }
        public override string NodeText => String.Format("{0} {1}", Desc, Name);
        public override IEnumerable<NodeBase> NodeChildren
        {
            get
            {
                var servers = new List<Server>();
                if (Server != null) servers.Add(Server);
                return servers;
            }
        }
    }
}
