﻿using SCDviewer.Serialization.IEDMeta.Logical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta
{
    public class Server : NodeBase
    {
        [XmlAttribute("desc")] public string Desc { get; set; }
        [XmlAttribute("timeout")] public int Timeout { get; set; }
        [XmlElement("Authentication")] public Authentication Auth { get; set; }
        [XmlElementAttribute("LDevice")] public List<LogicalDevice> LDevices { get; set; }

        public override string NodeText => Desc;
        public override IEnumerable<NodeBase> NodeChildren
        {
            get
            {
                return LDevices;
            }
        }
    }
}
