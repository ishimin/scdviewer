﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization
{
    public class ServerLink
    {
        [XmlAttribute("apName")] public string APName { get; set; }
        public override string ToString()
        {
            return APName;
        }
    }
}
