﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class Authentication
    {
        [XmlAttribute("none")] public bool None { get; set; }
        [XmlAttribute("password")] public bool Password { get; set; }
        [XmlAttribute("weak")] public bool Weak { get; set; }
        [XmlAttribute("strong")] public bool Strong { get; set; }
        [XmlAttribute("certificate")] public bool Certificate { get; set; }
    }
}
