﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class LogicalDevice : NodeBase
    {
        [XmlAttribute("inst")] public string Inst { get; set; }

        [XmlAttribute("desc")] public string Desc { get; set; }

        [XmlElementAttribute("LN0", typeof(LogicalNode0))]
        [XmlElementAttribute("LN", typeof(LogicalNode))]
        public List<LogicalNode> LogicalNodes { get; set; }
        [XmlElement("AccessControl")] public AccessControl AccessControl { get; set; }

        public override string NodeText => Desc;

        public override IEnumerable<NodeBase> NodeChildren
        {
            get
            {
                return LogicalNodes.Where(l => (l is LogicalNode0));
            }
        }
    }
}
