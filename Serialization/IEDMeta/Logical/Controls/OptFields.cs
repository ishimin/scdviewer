﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical.Controls
{
    public class OptFields
    {
        [XmlAttribute("bufOvfl")] public bool BufOvfl { get; set; }
        [XmlAttribute("reasonCode")] public bool ReasonCode { get; set; }
    }
}
