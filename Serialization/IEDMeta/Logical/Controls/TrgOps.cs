﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical.Controls
{
    public class TrgOps
    {
        [XmlAttribute("dchg")] public bool DCHG { get; set; }
        [XmlAttribute("gi")] public bool GI { get; set; }
        [XmlAttribute("period")] public bool Period { get; set; }
    }
}
