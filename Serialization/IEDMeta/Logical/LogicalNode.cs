﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    [Serializable]
    public class LogicalNode : NodeBase
    {
        [XmlAttribute("desc")] public string Desc { get; set; }
        [XmlAttribute("prefix")] public string Prefix { get; set; }
        [XmlAttribute("inst")] public string Inst { get; set; }
        [XmlAttribute("lnClass")] public string InClass { get; set; }
        [XmlAttribute("lnType")] public string InType { get; set; }
        [XmlElementAttribute("DOI")] public List<DOI> DOIs { get; set; }
        public override string NodeText => String.Format("LN: {0} prefix:{1} inst:{2}", Desc, Prefix, Inst);
        public override IEnumerable<NodeBase> NodeChildren
        {
            get
            {
                return null;
            }
        }
    }
}
