﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class DataSet : NodeBase
    {
        [XmlAttribute("name")] public string Name { get; set; }

        [XmlElementAttribute("FCDA")] public List<FCDA> FCDAs { get; set; }

        public override string NodeText => Name;

        public override IEnumerable<NodeBase> NodeChildren => FCDAs;
    }
}
