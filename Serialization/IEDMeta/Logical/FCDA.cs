﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class FCDA : NodeBase
    {
        [XmlAttribute("ldInst")] public string IDInst { get; set; }
        [XmlAttribute("prefix")] public string Prefix { get; set; }
        [XmlAttribute("lnClass")] public string InClass { get; set; }
        [XmlAttribute("lnInst")] public string InInst { get; set; }
        [XmlAttribute("doName")] public string DoName { get; set; }
        [XmlAttribute("fc")] public string FC { get; set; }

        public override string NodeText => String.Format("name: {0}, prefix: {1}, IDinst: {2}, InInst: {3}, InClass: {4}", DoName, Prefix, IDInst, InInst, InClass);

        public override IEnumerable<NodeBase> NodeChildren => null;
    }
}
