﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SCDviewer.Serialization.DataTypeTemplates;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class DataSetItem : NodeBase
    {
        public string IDInst { get; set; }
        public string Prefix { get; set; }
        public string InClass { get; set; }
        public string InInst { get; set; }
        public string DoName { get; set; }
        public string FC { get; set; }
        public ObservableCollection<DA> DAs { get; set; }
        public override string NodeText => String.Format("{0}{1}{3}.{2}", Prefix, InClass, DoName, IDInst);
        public override IEnumerable<NodeBase> NodeChildren => DAs;

        public DataSetItem()
        {
            DAs = new ObservableCollection<DA>();
        }
    }
}
