﻿using SCDviewer.Serialization.DataTypeTemplates;
using SCDviewer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class GSEControl : NodeBase
    {
        [XmlAttribute("name")] public string Name { get; set; }
        [XmlAttribute("datSet")] public string DataSet { get; set; }
        [XmlAttribute("desc")] public string Desc { get; set; }
        [XmlAttribute("type")] public string Type { get; set; }
        [XmlAttribute("confRev")] public string ConfRev { get; set; }
        [XmlAttribute("appID")] public string AppID { get; set; }
        [XmlElement("IEDName")] public IEDLink IEDLink { get; set; }
        public override string NodeText => String.Format("Goose: '{0}' desc: '{1}' type: {2}", Name, Desc, Type);
        public GooseDescriptor Descriptor { get; set; }
        public System.Collections.ObjectModel.ObservableCollection<DataSetItem> DataSetItems { get; set; }
        public override IEnumerable<NodeBase> NodeChildren
        {
            get
            {
                return null;
            }
        }

        public GSEControl()
        {
            DataSetItems = new System.Collections.ObjectModel.ObservableCollection<DataSetItem>();
        }
        public override void GrabInfo(Scl root)
        {
            var gse = Scl.FindFirst<AddressGSE>(root.Communication, p => p.CBName == Name);
            var ap = Scl.FindFirst<ConnectedAP>(root.Communication, p => p.GSE.Contains(gse));
            var ied = Scl.FindFirst<IED>(root, p => p.Name == ap.IEDName);
            var ln0 = Scl.FindFirst<LogicalNode>(ied, p => (p is LogicalNode0) && (p as LogicalNode0).GSEControls.Contains(this));
            var ldev = Scl.FindFirst<LogicalDevice>(ied, p => p.LogicalNodes.Contains(ln0));

            var mac = gse.Address.TryGetAddress("MAC-Address");
            var appid = gse.Address.TryGetAddress("APPID");
            var vlanp = gse.Address.TryGetAddress("VLAN-PRIORITY");
            var vlanid = gse.Address.TryGetAddress("VLAN-ID");

            Descriptor = new GooseDescriptor()
            {
                Ref = String.Format("{0}{3}/{1}$GO${2}", ap?.IEDName, ln0?.InClass, gse?.CBName, ldev?.Inst),
                DestinationMAC = mac?.Value,
                AppID = appid?.Value,
                VlanPriority = vlanp?.Value,
                VlanID = vlanid?.Value,
                GooseID = Name,
                ConfidurationRev = ConfRev,
                DatasetName = String.Format("{0}{3}/{1}${2}", ap?.IEDName, ln0?.InClass, DataSet, ldev?.Inst),
            };

            DataSetItems.Clear();
            (ln0 as LogicalNode0)?.DataSets.ForEach(p => { if (p.Name == DataSet) { AttachDatasetItem(root, p); } });
        }

        private void AttachDatasetItem(Scl root, DataSet dataSet)
        {
            dataSet.FCDAs.ForEach(fcda =>
            {
                var lnode = Scl.FindFirst<LNodeType>(root.DataTypeTemplates, p => (p as LNodeType).InClass == fcda.InClass);
                var Do = Scl.FindFirst<DO>(lnode, p => (p as DO).Name == fcda.DoName);
                var dot = Scl.FindFirst<DOType>(root.DataTypeTemplates, p => (p as DOType).ID == Do?.Type);

                var item = new DataSetItem()
                {
                    IDInst = fcda.IDInst,
                    Prefix = fcda.Prefix,
                    InClass = fcda.InClass,
                    InInst = fcda.InInst,
                    DoName = fcda.DoName,
                    FC = fcda.FC,
                    DAs = new System.Collections.ObjectModel.ObservableCollection<DA>(),
                };

                dot?.DAs.ForEach(da =>
                {
                    if (da.FC == fcda.FC) { item.DAs.Add(da); }
                });

                DataSetItems.Add(item);
            });
        }
    }
}
