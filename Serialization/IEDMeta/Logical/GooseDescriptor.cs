﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class GooseDescriptor : NodeBase
    {
        public String Ref { get; set; }
        public String DestinationMAC { get; set; }
        public String SourceMAC { get; set; }
        public String AppID { get; set; }
        public String GooseID { get; set; }
        public String DatasetName { get; set; }
        public String Enabled { get; set; }
        public String VlanID { get; set; }
        public String VlanPriority { get; set; }
        public String NeedsComissioning { get; set; }
        public String ConfidurationRev { get; set; }
        public override string NodeText => "GooseDescriptor";
        public override IEnumerable<NodeBase> NodeChildren => null;
    }
}
