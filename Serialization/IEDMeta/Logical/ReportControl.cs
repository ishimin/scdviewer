﻿using SCDviewer.Serialization.IEDMeta.Logical.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class ReportControl : NodeBase
    {
        [XmlAttribute("name")] public string Name { get; set; }
        [XmlAttribute("datSet")] public string DataSet { get; set; }
        [XmlAttribute("intgPd")] public string IntgPd { get; set; }
        [XmlAttribute("confRev")] public string ConfRev { get; set; }
        [XmlAttribute("rptID")] public string RptID { get; set; }
        [XmlElement] public TrgOps TrgOps { get; set; }
        [XmlElement] public OptFields OptFields { get; set; }
        [XmlElement] public RptEnabled RptEnabled { get; set; }
        public override string NodeText => String.Format("ReportControl Name:{0}, DataSet:{1}", Name, DataSet);
        public override IEnumerable<NodeBase> NodeChildren => null;
    }
}
