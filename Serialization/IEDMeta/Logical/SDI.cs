﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class SDI
    {
        [XmlAttribute("name")] public string Name { get; set; }
        [XmlElementAttribute("DAI")] public List<DAI> DAIs { get; set; }
    }
}
