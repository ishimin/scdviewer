﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    public class DAI
    {
        [XmlAttribute("name")] public string Name { get; set; }

        [XmlAttribute("valKind")] public string ValueKind { get; set; }

        [XmlElement("Val")] public string Value { get; set; }
    }
}
