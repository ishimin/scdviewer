﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Logical
{
    [Serializable]
    public class LogicalNode0 : LogicalNode
    {
        [XmlElementAttribute("DataSet")] public List<DataSet> DataSets { get; set; }
        [XmlElementAttribute("ReportControl")] public List<ReportControl> ReportControls { get; set; }
        [XmlElementAttribute("GSEControl")] public List<GSEControl> GSEControls { get; set; }
        public override string NodeText => "LN0";
        public override IEnumerable<NodeBase> NodeChildren => GSEControls;
    }
}
