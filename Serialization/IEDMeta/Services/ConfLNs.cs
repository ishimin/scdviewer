﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Services
{
    public class ConfLNs : ServiceBase
    {
        [XmlAttribute("fixPrefix")] public bool FixPrefix { get; set; }
        [XmlAttribute("fixLnInst")] public bool fixLnInst { get; set; }
    }
}
