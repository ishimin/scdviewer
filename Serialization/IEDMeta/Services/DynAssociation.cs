﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Services
{
    [Serializable]
    public class DynAssociation : ServiceBase
    {
        [XmlAttribute("max")] public string Max { get; set; }
    }
}
