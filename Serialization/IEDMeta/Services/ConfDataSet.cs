﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Services
{
    public class ConfDataSet : ServiceBase
    {
        [XmlAttribute("max")] public string Max { get; set; }
        [XmlAttribute("maxAttributes")] public string MaxAttributes { get; set; }
        [XmlAttribute("modify")] public bool Modify { get; set; }

    }
}
