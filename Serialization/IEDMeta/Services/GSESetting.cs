﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Services
{
    [Serializable]
    public class GSESetting : ServiceBase
    {
        [XmlAttribute("cbName")] public string CbName { get; set; }
        [XmlAttribute("datSet")] public string DataSet { get; set; }
        [XmlAttribute("appID")] public string AppID { get; set; }
        [XmlAttribute("dataLabel")] public string DataLabel { get; set; }
    }
}
