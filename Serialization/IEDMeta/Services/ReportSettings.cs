﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SCDviewer.Serialization.IEDMeta.Services
{
    public class ReportSettings : ServiceBase
    {
        [XmlAttribute("cbName")] public string CbName { get; set; }
        [XmlAttribute("datSet")] public string DataSet { get; set; }
        [XmlAttribute("rptID")] public string RptID { get; set; }
        [XmlAttribute("optFields")] public string OptFields { get; set; }
        [XmlAttribute("bufTime")] public string BufTime { get; set; }
        [XmlAttribute("trgOps")] public string TrgOps { get; set; }
        [XmlAttribute("intgPd")] public string IntgPd { get; set; }
    }
}
