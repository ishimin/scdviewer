﻿using SCDviewer.Serialization;
using SCDviewer.Serialization.IEDMeta;
using SCDviewer.Serialization.IEDMeta.Logical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SCDviewer.Selectors
{
    public class DatasetSelector : DataTemplateSelector
    {
        public DataTemplate DefaultTemplate { get; set; }
        public DataTemplate DatasetTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var selectedTemplate = DefaultTemplate;

            if (item is GSEControl) selectedTemplate = DatasetTemplate;

            return selectedTemplate;
        }
    }
}
