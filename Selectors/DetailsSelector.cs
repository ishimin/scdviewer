﻿using SCDviewer.Serialization;
using SCDviewer.Serialization.IEDMeta;
using SCDviewer.Serialization.IEDMeta.Logical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SCDviewer.Selectors
{
    public class DetailsSelector : DataTemplateSelector
    {
        public DataTemplate DefaultTemplate { get; set; }
        public DataTemplate ControlBlockTemplate { get; set; }
        public DataTemplate IEDTemplate { get; set; }
        public DataTemplate AccessPointTemplate { get; set; }
        public DataTemplate ServerTemplate { get; set; }
        public DataTemplate LDeviceTemplate { get; set; }
        public DataTemplate LN0Template { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var selectedTemplate = DefaultTemplate;

            if (item is GSEControl) selectedTemplate = ControlBlockTemplate;
            else if (item is IED) selectedTemplate = IEDTemplate;
            else if (item is AccessPoint) selectedTemplate = AccessPointTemplate;
            else if (item is Server) selectedTemplate = ServerTemplate;
            else if (item is LogicalDevice) selectedTemplate = LDeviceTemplate;
            else if (item is LogicalNode0) selectedTemplate = LN0Template;

            return selectedTemplate;
        }
    }
}
