﻿using SCDviewer.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;

namespace SCDviewer.ViewModel
{
    public class RootViewModel : ViewModelBase
    {
        #region properties
        
        static RootViewModel _this = null;
        public static RootViewModel This { get => _this; set => _this = value; }

        private Scl _scl = null;
        public Scl Scl
        {
            get { return _scl; }
            set { _scl = value; NotifyPropertyChanged(); }
        }

        string _logMessage;
        public string LogMessage
        {
            get
            {
                return _logMessage;
            }
            set
            {
                _logMessage = value; NotifyPropertyChanged();
            }
        }

        StringBuilder _messageBuilder = new StringBuilder();

        Dictionary<string, int> _unknownNodes = new Dictionary<string, int>();

        Dictionary<string, int> _unknownAttributes = new Dictionary<string, int>();

        #endregion

        #region commands

        private ICommand _openFile;
        public ICommand OpenFileCommand
        {
            get
            {
                if (_openFile == null)
                {
                    _openFile = new RelayCommand(
                        p => true,
                        p => this.OpenFile());
                }
                return _openFile;
            }
        }

        private ICommand _updateDetails;
        public ICommand UpdateDetailsCommand
        {
            get
            {
                if (_updateDetails == null)
                {
                    _updateDetails = new RelayCommand(
                        p => true,
                        p => this.UpdateDetails(p));
                }
                return _updateDetails;
            }
        }

        public void UpdateDetails(object p)
        {
            if (p is NodeBase)
                (p as NodeBase).GrabInfo(Scl);
        }

        #endregion

        #region logic
        public class NamespaceXmlTextReader : XmlTextReader
        {
            public NamespaceXmlTextReader(System.IO.TextReader reader) : base(reader) { }

            public override string NamespaceURI
            {
                get { return ""; }
            }
        }
        private void OpenFile()
        {
            try
            {
                var ofd = new Microsoft.Win32.OpenFileDialog();
                ofd.Filter = "SCD files (*.scd)|*.scd";
                if (ofd.ShowDialog() == true)
                {
                    _messageBuilder.Clear();
                    Scl = Deserialize(ofd.FileName);
                    LogMessage += "Разбор завершен.\n";
                }
            }
            catch (Exception ex)
            {
                _messageBuilder.Append(ExceptionText(ex));
            }
            finally
            {
                _messageBuilder.Append(UnknownNodesText());
                LogMessage += _messageBuilder.ToString();
            }
        }

        private Scl Deserialize(String path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Scl));
            serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            var reader = new NamespaceXmlTextReader(new StreamReader(path));
            return serializer.Deserialize(reader) as Scl;
        }

        private void serializer_UnknownAttribute (object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            var path = String.Format("{0}|\"{1}\"", e.ObjectBeingDeserialized, attr.Name);

            if (_unknownAttributes.ContainsKey(path))
                _unknownAttributes[path]++;
            else
                _unknownAttributes.Add(path, 1);
        }

        private void serializer_UnknownNode (object sender, XmlNodeEventArgs e)
        {
            var path = String.Format("{0}|\"{1}\"", e.ObjectBeingDeserialized, e.Name);

            if (_unknownNodes.ContainsKey(path))
                _unknownNodes[path]++;
            else
                _unknownNodes.Add(path, 1);
        }

        private string UnknownNodesText()
        {
            var message = new StringBuilder();
            if (_unknownNodes.Count > 0)
            {
                message.AppendLine("Unknown Nodes: ");
                var e = _unknownNodes.GetEnumerator();
                while (e.MoveNext())
                {
                    var pair = e.Current;
                    message.AppendLine(String.Format("{0}: {1}", pair.Key, pair.Value));
                }
            }

            if (_unknownAttributes.Count > 0)
            {
                message.AppendLine("Unknown Attributes: ");
                var e = _unknownAttributes.GetEnumerator();
                while (e.MoveNext())
                {
                    var pair = e.Current;
                    message.AppendLine(String.Format("{0}: {1}", pair.Key, pair.Value));
                }
            }
            return message.ToString();
        }

        private string ExceptionText(Exception ex, string prefix = "")
        {
            var result = new StringBuilder();
            result.AppendLine(prefix + ex.Message + "\n");

            if (ex.InnerException != null) result.Append(ExceptionText(ex.InnerException, prefix + "\t"));

            if (string.IsNullOrEmpty(prefix)) result.Append(prefix + ex.StackTrace);

            return result.ToString();
        }
        #endregion
    }
}
